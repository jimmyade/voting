const Sequelize = require('sequelize');
const db = require('../datab/db');
const { service_id } = require('../datab/service')

const Users = db.define('users', {
    id: {
        type: Sequelize.BIGINT(20),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    group_id: {
        type: Sequelize.BIGINT(20)
    },
    name: {
        type: Sequelize.STRING
    },
    username: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING(100)
    },
    user_phone: {
        type: Sequelize.STRING(20)
    },
    address: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    service_code: {
        type: Sequelize.STRING(10)
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    allowmobilelogin: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    },
    allowdesktoplogin: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    },
    first_use: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    },
    ministry_admin: {
        type: Sequelize.TINYINT(4),
        allowNull: true
    },
    surname: {
        type: Sequelize.STRING
    },
    firstname: {
        type: Sequelize.STRING
    },
    middlename: {
        type: Sequelize.STRING
    },

    prev_username: {
        type: Sequelize.STRING(45)
    },
    updated_by: {
        type: Sequelize.STRING
    },
    registered_on: {
        type: Sequelize.DATE
    },
    inactive: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    },
    created_by: {
        type: Sequelize.STRING(120)
    },
    reset_token: {
        type: Sequelize.TEXT
    },
    reset_expiry_date: {
        type: Sequelize.DATE
    },
    country_id: {
        type: Sequelize.INTEGER(11)
    },
    state_id: {
        type: Sequelize.INTEGER(11)
    },
    lga_id: {
        type: Sequelize.INTEGER(11)
    },
    lga_id: {
        type: Sequelize.INTEGER(11)
    },
    service_logo: {
        type: Sequelize.BLOB
    }
}, {
    freezeTableName: true,
    timestamps: false
});

const contestant = db.define('contestant', {
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    names: {
        type: Sequelize.STRING
    },
    state_code: {
        type: Sequelize.STRING
    },
    callup_number: {
        type: Sequelize.STRING
    },
    graduated_year: {
        type: Sequelize.STRING
    },
    school: {
        type: Sequelize.STRING
    },
    discipline: {
        type: Sequelize.STRING
    },
    department: {
        type: Sequelize.STRING
    },
    approval: {
        type: Sequelize.TINYINT(1)
    },
    approve_by: {
        type: Sequelize.STRING
    },
    stream: {
        type: Sequelize.STRING
    },
    batch: {
        type: Sequelize.STRING
    },
    registereddate: {
        type: Sequelize.DATE
    },
    photo: {
        type: Sequelize.TEXT
    },
    email: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.STRING
    },
    gender: {
        type: Sequelize.STRING
    },
    contest_for: {
        type: Sequelize.INTEGER
    },
    platoon: {
        type: Sequelize.INTEGER
    }
}, {
    freezeTableName: true,
    timestamps: false
});

const polls = db.define('polls', {
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: Sequelize.TEXT
    },
    description: {
        type: Sequelize.TEXT
    },
    contestant_id: {
        type: Sequelize.INTEGER
    },
    voter_name: {
        type: Sequelize.STRING
    },
    amount_paid: {
        type: Sequelize.DECIMAL(50)
    },
    status: {
        type: Sequelize.TINYINT(1)
    },
    castdate: {
        type: Sequelize.DATE
    },
}, {
    freezeTableName: true,
    timestamps: false
});


const current_batch_stream = db.define('current_batch_stream', {
    current_batch: {
        type: Sequelize.STRING
    },
    current_stream: {
        type: Sequelize.STRING
    },
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    }
}, {
    freezeTableName: true,
    timestamps: false
});


const ballot = db.define('ballot', {
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    poll_id: {
        type: Sequelize.STRING
    },
    title: {
        type: Sequelize.STRING
    },
    votes: {
        type: Sequelize.STRING
    },
    contesttant: {
        type: Sequelize.STRING
    },
    contestant_id: {
        type: Sequelize.STRING
    },
    datecast: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.TINYINT 
    },
    reference: {
        type: Sequelize.STRING
    },
    voters_email: {
        type: Sequelize.STRING
    },
    amount: {
        type: Sequelize.DECIMAL(50)
    },
}, {
    freezeTableName: true,
    timestamps: false
});
module.exports = {
    Users, contestant, polls, current_batch_stream, ballot

};
