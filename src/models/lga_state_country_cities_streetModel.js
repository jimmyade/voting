const Sequelize = require('sequelize');
const db = require('../datab/db');


const _lgas = db.define('_lga',{
    lga_id:{
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    lga:{
        type: Sequelize.STRING(50),
    },
    lga_code:{
        type: Sequelize.STRING
    },
    state_id:{
        type: Sequelize.BIGINT(20),
    }
},{
    timestamps: false,
    freezeTableName: true
});

const _countries = db.define('_countrie',{
    country_id:{
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    country:{
        type: Sequelize.STRING(45),
    },
    country_code:{
        type: Sequelize.STRING(45)
    },
}
,{
    timestamps: false
});

const _states = db.define('_state',{
    state_id:{
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    state:{
        type: Sequelize.STRING(45),
    },
    state_code:{
        type: Sequelize.STRING(45)
    },
    country_id:{
        type: Sequelize.INTEGER(20),
    }
},{
    timestamps: false
});

const _residential_address_status = db.define('_residential_address_statu', {
    residential_address_status_id:{
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    residential_status:{
        type: Sequelize.STRING(45),
        allowNull: true
    }
},{
    timestamps: false
});

const _cities = db.define('_cities',{
    city_id:{
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    city:{
        type: Sequelize.STRING(45),
    },
    city_code:{
        type: Sequelize.STRING(45)
    },
    lga_id:{
        type: Sequelize.BIGINT(20),
    },
    created_at:{
        type: Sequelize.DATE,
    },
    updated_at:{
        type: Sequelize.DATE,
    }
},{
    timestamps: false
});

const titletable = db.define('titletable', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    titleName: {
        type: Sequelize.STRING(45),
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    registeredBy: {
        type: Sequelize.STRING(95)
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const _designations = db.define('_designations', {
    designation_id:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    designation: {
        type: Sequelize.STRING
    },
    code: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const pat_diagnosis = db.define('pat_diagnosis', {
    DiagID:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    DiaDetail: {
        type: Sequelize.STRING
    },
    DiagRemark: {
        type: Sequelize.STRING
    },
    DiagDate: {
        type: Sequelize.DATE
    },
    Description: {
        type: Sequelize.STRING
    },
    PatID: {
        type: Sequelize.INTEGER
    },
    DiaReportDoc:{
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const prescription = db.define('medicine', {
    MedID:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    MedName: {
        type: Sequelize.STRING
    },
    MedInstruction: {
        type: Sequelize.STRING
    },
    MedPrice: {
        type: Sequelize.DECIMAL
    },
    DiagID: {
        type: Sequelize.STRING
    },
    PatID: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const Patients = db.define('patients', {
    PatID:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    PatName: {
        type: Sequelize.STRING
    },
    PatEmail: {
        type: Sequelize.STRING
    },
    PatPhone: {
        type: Sequelize.STRING
    },
    RegDate: {
        type: Sequelize.DATE
    },
    PatCountry: {
        type: Sequelize.DATE
    },
    PatState: {
        type: Sequelize.STRING
    },
    PatLGA: {
        type: Sequelize.STRING
    },
    PaAddress: {
        type: Sequelize.STRING
    },
    photo: {
        type: Sequelize.STRING
    },
    dob: {
        type: Sequelize.DATEONLY
    },
    gender: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const billing = db.define('billing', {
    BillID:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    PatID: {
        type: Sequelize.STRING
    },
    BillAmount: {
        type: Sequelize.DECIMAL
    },
    BillDate: {
        type: Sequelize.DATE
    },
    settled: {
        type: Sequelize.TINYINT
    },
    approved: {
        type: Sequelize.TINYINT
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const user_groups = db.define('user_groups', {
    idgroups:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    group_id: {
        type: Sequelize.INTEGER(11)
    },
    group_name: {
        type: Sequelize.STRING
    },
    
}, {
    timestamps: false,
    freezeTableName: true
})

module.exports={
    _countries,
    _states,
    _residential_address_status,
    _lgas, user_groups,
    _cities, titletable, _designations, prescription, pat_diagnosis, Patients, billing
}