const express = require('express');
const multer = require('multer');
const passport = require('passport');
var bcrypt = require('bcryptjs');;
const saltRounds = 10;
const { _states, _countries, _lgas, _designations } = require('../../models/lga_state_country_cities_streetModel');
const { Users } = require('../../models/usersMosel');
//const { _cities, _countries,_lgas}= require('../../models/taxPayersModels/texPayersmodels')

const appUsers = express.Router();
// function for uploading picture via multer
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, fulse)
  }
}
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2
  },
  fileFilter: fileFilter
});


function redirectIfLoggedIn(req, res, next) {
  if (req.user) return res.redirect('/users/pauls_dashboard');
  return next();
}

// module.exports = () => {
appUsers.post('/login', passport.authenticate('local', {
    failureFlash: true,
    failureRedirect: '/login'
  }), (req, res, next) => {
    if(req.session.oldUrl) {
      res.redirect(req.session.oldUrl);
      req.session.oldUrl = null;
    } else {
      res.redirect('/success')
    }
  });
appUsers.get('/login', (req, res, next) => {
  res.render('./admin/login');
})


appUsers.get('/success', async(req, res) => {
  if(req.user.group_id = 111111 ) {
     res.redirect('/api/dashboard')
  } else if(req.user.group_id = 222222 ) {
    res.redirect('/dashboard')
  } else {


  }
})



appUsers.get('/logout', (req, res, next) => {
  req.logout();
  // req.session.oldUrl = null;
  req.session.destroy();
  
  res.redirect('/')
});

appUsers.get('/', async(req, res) => {
  res.render('./admin/index');
})

appUsers.get('/registration', (req, res, next) => {
  _countries.findAll().then(function (country) {
    res.render('./admin/registration', {
      success: "",
      country: country,
      err: "",
      Error: ""
    });
  });
});

// get state 
appUsers.get('/api/getstate/:country_id', (req, res) => {
  let country_id = parseInt(req.params.country_id);
  _states.findAll({ where: { country_id: country_id } }).then(function (result) {
    res.send(result);
  })
});
//get lga 
appUsers.get('/api/getlga/:stateID', (req, res) => {
  let stateID = parseInt(req.params.stateID);
  _lgas.findAll({ where: { state_id: stateID } }).then(function (result) {
    res.send(result);
  })
});
// get city 
appUsers.get('/api/getcity/:lgaID', (req, res) => {
  let lgaID = parseInt(req.params.lgaID);
  _cities.findAll({ where: { lga_id: lgaID } }).then(function (result) {
    res.send(result);
  })
});






appUsers.post('/registration', upload.single('service_logo'), async (req, res, next) => {
  try {

  
    const surname = req.body.surname;
    const firstname = req.body.firstname;
    const user_phone = req.body.phone;
    const email = req.body.email;
    const country = req.body.country_id;
    const state = req.body.state_id;
    const lga = req.body.lga_id;

    const address = req.body.address;

    const kin_names = req.body.kin_name;
    const kin_phone = req.body.kin_phone;
    const kin_email = req.body.kin_email;

  
    // const password =  
  
    const service_logo = req.file.filename;
    var Error = [];

    await CreateNewClient({ user_phone, email, country, state, lga, surname, firstname, address, kin_names, kin_phone, kin_email, service_logo});

  } catch (err) {
    res.render("./admin/registration", {
      err: err,
      country: '',
      Error: ''
    });
  }

  async function _Encrypt(text) {
    return await bcrypt.hash(text, saltRounds);
  }

  async function CreateNewClient(args) {
    if (args.password) {
      args.password = await _Encrypt(args.password);
    }
    
    return await CreateClient(args)
  }
  async function CreateClient(args) {
    try {
      if (!args.surname) Error.push({ msg: 'Invalid argument last name' });
      if (!args.firstname) Error.push({ msg: 'Invalid argument first_name' });
      if (!args.user_phone) Error.push({ msg: 'Invalid argument phone' });
      if (!args.email) Error.push({ msg: 'Invalid argument email' });
      if (!args.country) Error.push({ msg: 'Invalid argument country' });

      if (!args.state) Error.push({ msg: 'Invalid argument state' });
      if (!args.lga) Error.push({ msg: 'Invalid argument lga' });
      if (!args.address) Error.push({ msg: 'Invalid argument address' });
      if (!args.kin_names) Error.push({ msg: 'Invalid argument kin Name' });

      if (!args.kin_phone) Error.push({ msg: 'Invalid argument kin Phone' });
      if (!args.kin_email) Error.push({ msg: 'Invalid argument kin email' });
      
      if (!args.service_logo) Error.push({ msg: 'Invalid argument service_logo' });
      if (Error.length > 0) {
        res.render("./admin/registration", {
          err: "",
          country: '',
          Error: Error
        });
      } else {
        let Registration = new Users({
          group_id: 111111,
          surname: args.surname,
          firstname: args.firstname,
          user_phone: args.user_phone,
          email: args.email,
          country: args.country,
          state: args.state,
          lga: args.lga,
          address: args.address,
          kin_names: args.kin_names,
          kin_phone: args.kin_phone,
          kin_email: args.kin_email,
          password: await bcrypt.hash("1234567", saltRounds),
          username: args.email,
          name: `${args.surname} ${args.firstname}`,
          service_logo: args.service_logo
        });
        
        Registration.save().then(services => {
          req.flash('success', ` Created`);
          res.redirect('/login')
        }).catch(err => {
          console.log(err)
          res.render("./admin/registration", {
            err: err.message,
            country: '',
            Error: Error
          });
        });

      }
    } catch (err) {
      req.flash('success', err.message);
      res.redirect('back')
    }
  }
});

// return appUsers;
// };

module.exports = appUsers
