const express = require('express');
//const flash = require('connect-flash');
const userRoute = express.Router();

const bcrypt = require('bcryptjs');
//const passport = require('passport');

//include user model
const Client_service = require('../../models/clientServiceModel/clientServiceModel')

userRoute.get('/login', (req, res, next)=>{
  res.render('./Users/login',{
       errors:"",
        tit: "",
  })
})

userRoute.get('/user_registration', (req, res, next)=>{
    res.render('./Users/user_registration',{
         errors:"",
          tit: "",
    })
})
userRoute.post('/user_registration', (req, res, next)=>{
    const { admin_firstname, admin_middlename, admin_surname, client_admin_phone, client_admin_email, client_admin_pwd, client_admin_pwd2 } = req.body;
    let errors = [];
    if(!admin_firstname || !admin_middlename || !admin_surname|| !client_admin_phone || !client_admin_email || !client_admin_pwd || !client_admin_pwd2){
        errors.push({msg : 'Please enter all fields'})
    }
    if (client_admin_pwd != client_admin_pwd2) {
        errors.push({ msg: 'Passwords do not match' });
      }
    
      if (client_admin_pwd.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters' });
      }
      if (errors.length < 0) {
        res.render('./Users/user_registration', {
          errors:errors,
          tit: "",
          admin_firstname,
          admin_middlename,
          admin_surname,
          client_admin_phone,
          client_admin_email,
          client_admin_pwd,
          client_admin_pwd2
        });
        //console.log(errors)
      } else {
        Client_service.findAll({where:{ client_admin_email: client_admin_email }}).then(client_service => {
          if (client_service) {
            errors.push({ msg: 'Username already exists' });
            res.render('./Users/user_registration', {
                errors,
                tit: "",
                admin_firstname,
                admin_middlename,
                admin_surname,
                client_admin_phone,
                client_admin_email,
                client_admin_pwd,
                client_admin_pwd2
            });
            //console.log(errors)
          } else {
            let newClient_service = new Client_service({
              admin_firstname,
              admin_middlename,
              admin_surname,
              client_admin_phone,
              client_admin_email,
              client_admin_pwd
            });
            //console.log(newUser)
            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newClient_service.client_admin_pwd, salt, (err, hash) => {
                if (err) throw err;
                newClient_service.client_admin_pwd = hash;
                newClient_service.save().then(client_service => {
                  client_service.findOne({where: {client_admin_email:newClient_service.client_admin_email}}).then(realuser=>{
                    //req.login(user.userid, function(err){
                      var userid = realuser.userid
                      res.redirect(`/login`);
                    //});
                  }).catch(err => console.log(err));
                })
                  
              });
            });
          }
        });
      }
});

module.exports = userRoute