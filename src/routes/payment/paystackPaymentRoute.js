const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const _ = require('lodash');
const path = require('path');
const paystacts = express.Router();

const db = require('../../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
// const { Payment, Orders, Ordersproduct, Products } = require('../../models/productModel/productmodel');
const { Users, ballot } = require('../../models/usersMosel');

// const { Donation } = require('../../models/normalize_model');


const { initializePayment, verifyPayment } = require('../../lib/paystack')(request);



paystacts.post('/payment', (req, res) => {
    try {
        const form = _.pick(req.body, ['amount', 'email', 'full_name']);
        form.metadata = {
            full_name: form.full_name
        }
        form.amount *= 100;
        initializePayment(form, (error, body) => {
            if (error) {
                //handle errors
                console.log(error);
                return;
            }
            response = JSON.parse(body);
            console.log(response);

            if (response.data.authorization_url) {
                var c = parseInt(req.body.amount) / 10
                let paymentdata = {
                    poll_id: req.body.poll_id,
                    title: req.body.title,
                    votes: req.body.votes,
                    contesttant: req.body.full_name,
                    contestant_id: req.body.contestant_id,
                    voters_email: req.body.email,
                    amount: req.body.amount,
                    reference: response.data.reference
                };
                let newVote = new ballot(paymentdata)


                console.log(paymentdata)
                newVote.save()
                res.json({
                    msg: "success",
                    result: response.data.authorization_url
                })
                // res.redirect(response.data.authorization_url)
            }

        });
    } catch (e) {
        req.json({ msg: e.message })
    }


});



paystacts.get('/paystack/callback', async (req, res) => {
    const ref = req.query.reference;
    verifyPayment(ref, (error, body) => {
        if (error) {
            //handle errors appropriately
            console.log(error)
            return res.redirect('/error');
        }
        response = JSON.parse(body);

        const data = _.at(response.data, ['reference', 'amount', 'customer.email', 'metadata.full_name']);

        [reference, amount, email, full_name] = data;
        console.log("this is a return data " + data)
        newDonor = { references: reference, amount, email, full_name }
        // console.log(newDonor)
        let updatedet = {
            status: 1
        }
        ballot.update(updatedet, { where: { reference: ref } }).then(ress => {
            db.query(`SELECT
            *
        FROM
            ballot
        INNER JOIN contestant ON contestant.id = ballot.contestant_id
        INNER JOIN polls ON polls.id = ballot.poll_id
        WHERE
            ballot.reference = '${ref}'
        and ballot.status = 1`, { type: QueryTypes.SELECT }).then(function (result) {
            console.log(result)
                res.render('./admin/print_success', {
                    result:result[0]
                })
            })
            // res.send(ress)
        })
        console.log('done')
    })
});


paystacts.get('/payment_notification', (req, res) => {
    res.render('./admin/print_success')
})
paystacts.get('/receipt/:id/:reference', (req, res) => {
    const id = req.params.id;
    const reference = req.params.reference;
    Ordersproduct.findAll({ where: { paymentRef: reference } }).then(result => {
        Payment.findOne({ where: { references: reference } }).then(details => {
            res.render('./home/payment/payment_invoice', {
                result: result,
                details: details
            })
        })
    })
})

paystacts.get('/error', (req, res) => {
    res.render('', {

    })
})


module.exports = paystacts;