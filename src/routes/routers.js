"use strict";
const express = require('express');
//const flash = require('connect-flash');
require('dotenv').config()
const multer = require('multer');
const resizeImg = require('resize-img');
const fs = require('fs');
const fse = require('fs-extra');
const request = require('request');

const nodemailer = require("nodemailer");
const bcrypt = require('bcryptjs');
const saltRounds = 10;

const db = require('../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;

const route = express.Router();

const { sign, verify } = require("jsonwebtoken");

const { _countries, _states, _lgas } = require('../models/lga_state_country_cities_streetModel');
const { Users, contestant, polls, current_batch_stream } = require('../models/usersMosel');

let accessControl = require('../lib/service');
// const { now } = require('sequelize/types/lib/utils');
let adminAccess = accessControl.isAdmin;
let contAccess = accessControl.isContestant;
let transporter = nodemailer.createTransport({

  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: "safemedi037@gmail.com",
    pass: "09011459751"
  }
});


// Please do not enable 2 factor authentication on that email, all the requires configuration are in place.
// that means you are all good to go

// function for uploading picture via multer
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, "malaika" + Date.now() + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    req.fileValidationError = "Only JPEG, JPG And PNG file allowed";
    cb(null, fulse)
  }
}
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});


let y = new Date();
let year = y.getFullYear()

route.get('/api/dashboard', adminAccess, async (req, res, next) => {

  res.render('./admin/adminPages/index')

});
// view images

route.route('/staff_enrollment')
  .get(adminAccess, async (req, res) => {
    res.render('./admin/applicant/staff_reg')
  })
  .post(async (req, res) => {
    try {
      console.log(req.body)
      let u = {
        name: req.body.name,
        username: req.body.email,
        email: req.body.email,
        user_phone: req.body.phone_number,
        reason: req.body.address,
      }
      let staffUser = new Users(u)
      staffUser.save().then((result) => {
        res.json({
          msg: "success",
          result: result
        })
      })
    } catch (e) {
      res.json({
        msg: e.message
      })
    }


  })




route.route('/create_contestant')
  .get(adminAccess, async (req, res) => {
    let poll = await polls.findAll({
      where: {
        status: 1,
        andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('castdate')), year)
      }
    })
    res.render('./admin/contestants/create_contestant_account', {
      poll
    })
  })
  .post(adminAccess, async (req, res) => {
    try {
      console.log(req.body)
      let newContestant = new contestant(req.body);
      newContestant.save().then(function (result) {
        contestLogin(req, res)
        res.json({
          msg: "success"
        })
      })
    } catch (e) {
      res.json({ msg: e.message });
    }
  })

async function contestLogin(req, res) {
  var userd = {
    group_id: 222222,
    name: req.body.names,
    username: req.body.email,
    user_phone: req.body.phone,
    email: req.body.email,
    password: await bcrypt.hash(process.env.DEFAULT_PASSWORD, saltRounds)
  };
  let newUser = new Users(userd);
  newUser.save()
}

route.get('/search_contestant', adminAccess, async (req, res) => {
  res.render('./admin/adminPages/search_contestant')
})
route.get('/search_contestant_result', adminAccess, async (req, res) => {
  try {
    var batch = req.query.batch;
    var stream = req.query.stream;
    var year = req.query.year;

    let contest = await contestant.findAll({
      where: {
        stream: stream,
        batch: batch,
        andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('registereddate')), year)
      }
    });

    res.json({
      result: contest,
      msg: 'success'
    })
  } catch (e) {
    res.json({
      msg: e.message
    })
  }

})

route.route('/mgt_vote_positions')
  .get(adminAccess, async (req, res) => {
    let poll = await polls.findAll({
      where: {
        status: 1,
        andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('castdate')), year)
      }
    })
    console.log(poll)
    res.render('./admin/adminPages/poll', {
      poll
    })
  })
  .post(async (req, res) => {
    try {
      let newPoll = new polls(req.body)
      newPoll.save().then((result) => {
        res.json({
          msg: "success",
          result: result
        })
      })
    } catch (e) {
      res.json({
        msg: e.message,
      })
    }

  })

// contestant login detils
route.get('/dashboard', contAccess, async (req, res, next) => {

  let contest = await contestant.findOne({
    where: {
      email: req.user.email
    }
  });
  // console.log(contest)
  res.render('./admin/contestants/index', {
    contest
  })
})


route.get('/contestant', async (req, res) => {
  let CBS = await current_batch_stream.findOne({
    status: 1,
    andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('datecreated')), year)
  });
  let poll = await polls.findAll({
    where: {
      status: 1,
      andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('castdate')), year)
    }
  })
  let contests = await contestant.findAll({
    where: {
      stream: CBS.current_stream,
      batch: CBS.current_batch,
      andOp: Sequelize.where(Sequelize.fn('YEAR', Sequelize.col('registereddate')), year)
    }
  });
  res.render('./admin/contestant', {
    poll, contests
  })
})

route.route('/vote')
  .get(async (req, res) => {
    let poll = await polls.findOne({ where: { id: req.query.pollid } })
    let contest = await contestant.findOne({ where: { id:  req.query.contestantid} });
    res.render('./admin/vote_check_point', {
      poll, contest
    })
  })
route.get('/gallery', async (req, res, next) => {
  var gallery = await Gallery.findAll({ order: [['id', 'DESC']], limit: 100, offset: 0 })
  res.render('./home/gallery', {
    gallery: gallery
  })
});

// admin create users account  page
route.route('/admin/get_events')
  .get(async (req, res) => {
    let contry = await _countries.findAll();
    let event = await Programe.findAll();
    res.render('./admin/adminPages/events', {
      contry: contry,
      event: event
    });
  })
  .post(upload.single('Image'), async (req, res) => {
    try {
      let event = {
        Event_Title: req.body.Event_Title,
        Country: req.body.Country,
        State: req.body.State,
        LGA: req.body.LGA,
        short_des: req.body.short_des,
        Address: req.body.Address,
        Image: req.file.filename,
        Event_Description: req.body.Event_Description
      }
      var NewEvent = new Programe(event);
      NewEvent.save().then(function (result) {
        var path = 'uploads/' + req.file.filename;
        var saveimg = 'uploads/' + req.file.filename;

        resizeImg(fs.readFileSync(path), { width: 800, height: 800 }).then(function (buf) {
          fs.writeFileSync(saveimg, buf);
        })
        req.flash('info', 'Event Addess')
        res.redirect('back')
      })
    } catch (e) {
      req.flash('warning', e.message)
      res.redirect('back')
    }

  })


route.get('/api_admin/view_events', async (req, res) => {
  Programe.findAll({ order: [['Event_id', 'DESC']] }).then(function (result) {
    res.render('./admin/adminPages/view_events', {
      events: result
    })
  })
})

route.get('/api/delete_event', (req, res) => {
  Programe.findOne({ where: { event_id: req.query.id } }).then(event => {
    event.destroy().then(result => {
      var fileName = `./uploads/${event.imgUrl}`;
      fs.unlink(fileName, function (err) {
        if (err) throw err;
        // if no error, file has been deleted successfully
        req.flash('info', 'event deleted!')
        res.redirect('back');
      });

    })
  })
})

route.route('/admin/add_img_to_gallery')
  .get((req, res) => {
    res.render('./admin/adminPages/gallery')
  })
  .post(upload.array("file", 30), (req, res) => {
    let images = req.files;
    if (images.length > 0) {

      let uploads = [];
      images.forEach((i) => {
        var path = 'uploads/' + i.filename;
        var saveimg = 'uploads/' + i.filename;
        // resize the image
        resizeImg(fs.readFileSync(path), { width: 800, height: 600 }).then(function (buf) {
          fs.writeFileSync(saveimg, buf);

        })
        // move the image from original destination to the gallery folder
        var src = 'uploads/' + i.filename;
        var dest = 'uploads/gallery/' + i.filename;
        fse.move(src, dest, { overwrite: true })
        let upload = {
          img: i.filename
        };
        let mm = new Gallery(upload);
        mm.save().then(function (result) {
          res.sendStatus(200)
        })
        // uploads.push(upload);
      });
      // console.log(uploads)
      // Gallery.bulkCreate(uploads).then(function(result){
      //   res.sendStatus(200)
      // })
    }
  })

// videos
route.get('/admin/view_youtube_videos', (req, res) => {
  Videos.findAll().then(function (result) {
    res.render('./admin/adminPages/view_videos', {
      result: result
    })
  })
})
route.get('/delete/view_youtube_videos', (req, res) => {
  Videos.findOne({ where: { VideoId: req.query.id } }).then(function (result) {
    result.destroy().then(function (succ) {
      req.flash('info', "video deleted !");
      res.redirect('back')
    })
  })
})
route.route('/admin/em_youtube_video')
  .get(async (req, res) => {
    res.render('./admin/adminPages/add_video')
  })
  .post((req, res) => {
    var vid = {
      VideoTitle: req.body.VideoTitle,
      VideoIngURL: req.body.VideoIngURL,
      VideoURL: req.body.VideoURL
    };
    let video = new Videos(vid);
    video.save().then(function (result) {
      res.send(result)
    })
  })
route.route('/contact_us')
  .get((req, res) => {
    res.render('./home/contact_us')
  })
  .post((req, res) => {
    try {
      var info = {
        names: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        subject: req.body.subject,
        message: req.body.message
      }
      let NewContact = new Contact(info);
      NewContact.save().then(function (result) {
        res.json({ msg: "success" })
      })
    } catch (err) {
      console.log(err)
      res.json({ msg: "err" })

    }

  })

route.get('/admin/view_contact_message', (req, res) => {
  Contact.findAll({ order: [['id', 'DESC']] }).then(contacts => {
    res.render('./admin/adminPages/video_contacts', {
      contacts: contacts
    })
  })
})



// applicant
route.route('/aplicant_registration')
  .get(async (req, res) => {
    applicantRegController.getApplicatepage(req, res);
  })
  .post(upload.single('photo'), async (req, res) => {
    applicantRegController.createApplicant(req, res);
  })
route.get('/aplicant_profile', async (req, res) => {
  applicantRegController.viewStaffDetails(req, res);
})
route.route('/aplicant_update_details')
  .get(async (req, res) => {
    applicantRegController.getStaffUpdatePage(req, res);
  })
  .post(async (req, res) => {
    applicantRegController.updateStaffinfo(req, res);
  });

route.get('/aplicant_update_details', async (req, res) => {
  applicantRegController.viewStaffDetails(req, res);
})
// parents account
route.route('/patient_registration')
  .get(async (req, res) => {
    applicantRegController.parentLogin(req, res);
  })
  .post(upload.single('passp'), async (req, res) => {
    applicantRegController.createParentLogin(req, res);
  })

route.route('/daignosis')
  .get(async (req, res) => {
    applicantRegController.getDiagnosis(req, res)
  })
  .post(upload.single('diagnreport'), async (req, res) => {
    applicantRegController.addDiagnosis(req, res)
  })

route.route('/prescribe_drugs')
  .get(async (req, res) => {
    applicantRegController.getPrescription(req, res)
  })
  .post(async (req, res) => {
    applicantRegController.addPrescription(req, res)
  });

route.route('/view_drugs')
  .get(async (req, res) => {
    applicantRegController.getPrescription(req, res)
  })

// var unrest = require('unrest');

route.route('/view_diagnose_medical_report')
  .get(async (req, res) => {
    // var
    res.render('./admin/parents/patient_view_diagnosis')

  })
  .post(async (req, res) => {
    try {
      // let userOtp = await Users.findOne({ where: { usernme: req.user.usernme } })
      // console.log(req.user.otp)
      const validToken = verify(req.user.otp, process.env.JSON_SECRECT);

      let patient = await Patients.findOne({ where: { PatEmail: req.user.email } })


      console.log(validToken.otp)
      console.log(req.body.password)
      if (validToken.otp = req.body.password) {

        let diagnosis = await pat_diagnosis.findAll({ where: { PatID: patient.PatID } });
        res.render('./admin/parents/diagnoze_patient', {
          diagnosis, ID: patient.PatID, patient
        })
      } else {
        req.flash('danger', "SORRY THE OTP IS INCORRECT, please enter the correct number sent to you.");
        res.redirect('back')
      }
    } catch (err) {
      console.log(err)
    }


  })

route.get('/resend_sms', async (req, res) => {

  function OTP(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
      n = Math.floor(Math.random() * 7);
      s += nuc[n];
      i++;
    }
    return s;
  };
  var oneTimePassword = OTP(6);
  const accessToken = sign(
    {
      username: req.user.username,
      id: req.user.id,
      group_id: req.user.group_id,
      name: req.user.name,
      user_phone: req.user.user_phone,
      service_id: req.user.service_id,
      service_code: req.user.service_code,
      otp: oneTimePassword
    },
    process.env.JSON_SECRECT
  );

  console.log(accessToken)
  let patient = await Patients.findOne({ where: { PatEmail: req.user.email } })
  var message = `Dear ${patient.PatName}, your OTP is ${oneTimePassword}`;
  var phoneNumber = patient.PatPhone;
  request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=YAFD&to=${phoneNumber}&body=${message}&dnd=2`, function (error, ress, body) {
    console.error('error:', error); // Print the error if one occurred
    console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
    console.log('body:', body); // Print the HTML for the Google homepage.
  });
  // let otphash = await bcrypt.hash(oneTimePassword, saltRounds);
  Users.update({ otp: accessToken }, { where: { email: req.user.email } }).then(function (result1) {
    req.flash('info', "OTP has been sent to you as SMS")
    res.render('./admin/parents/patient_view_diagnosis')
    // res.send(accessToken)
  })

})


route.route('/asign_doctor_to_patient')
  .get(async (req, res) => {
    let patient = req.query.PatID;
    let diagnosisid = req.query.DiagID;
    let staffs = await staffregistration.findAll()
    let staffid = await staffregistration.findOne({ where: { personal_phone_no: req.user.user_phone } })
    let assigns = await db.query(`SELECT * from assignment INNER JOIN staffregistration on staffregistration.staff_reg_id = assignment.staff_id
    INNER JOIN patients on patients.PatID =  assignment.patientid
    INNER JOIN pat_diagnosis on pat_diagnosis.DiagID = assignment.diagnosisid  WHERE assignment.staff_id = ${staffid.staff_reg_id};`, { type: QueryTypes.SELECT });
    res.render('./admin/parents/assign_staff_to_patient', {
      staffs, ID: req.query.PatID, diagnosisid, assigns
    })
  })
  .post(async (req, res) => {
    try {
      let patient = req.query.PatID;
      let diagnosisid = req.query.DiagID;
      let users = await staffregistration.findOne({ where: { staff_reg_id: req.body.staff_id } })

      let assign = {
        patientid: patient,
        diagnosisid: diagnosisid,
        staff_id: users.staff_reg_id
      };

      console.log(assign)

      let newAssign = new assignment(assign);
      newAssign.save().then(function (result) {
        req.flash('info', 'Assigned Successfully');
        res.redirect('back')
      })
    } catch (e) {
      req.flash('danger', e.message);
      res.redirect('back')
    }
  })

route.get('/view_assigned_staffs', async (req, res) => {
  let patient = req.query.PatID;
  let diagnosisid = req.query.DiagID;
  let staffs = await staffregistration.findAll()
  let assigns = await db.query(`SELECT * from assignment INNER JOIN staffregistration on staffregistration.staff_reg_id = assignment.staff_id
  INNER JOIN patients on patients.PatID =  assignment.patientid
  INNER JOIN pat_diagnosis on pat_diagnosis.DiagID = assignment.diagnosisid WHERE assignment.patientid = ${patient};`, { type: QueryTypes.SELECT });
  res.render('./admin/parents/assign_staff_to_patient', {
    staffs, ID: req.query.PatID, diagnosisid, assigns
  })


})

route.get('/admin/asign_doctor_to_patient', async (req, res) => {
  let patient = req.query.PatID;
  let diagnosisid = req.query.DiagID;
  let staffs = await staffregistration.findAll()
  let assigns = await db.query(`SELECT * from assignment INNER JOIN staffregistration on staffregistration.staff_reg_id = assignment.staff_id
  INNER JOIN patients on patients.PatID =  assignment.patientid
  INNER JOIN pat_diagnosis on pat_diagnosis.DiagID = assignment.diagnosisid WHERE assignment.patientid = ${patient};`, { type: QueryTypes.SELECT });
  res.render('./admin/parents/assign_staff_to_patient', {
    staffs, ID: req.query.PatID, diagnosisid, assigns
  })


})


route.route('/send_email_messages')
  .get(async (req, res) => {
    res.render('./admin/parents/send_email', {
      email: req.query.email_address,
      name: req.query.name,
      staff_id: req.query.staff_registration_id
    })
  })
  .post(async (req, res) => {

    var mailOptions = {
      from: 'safemedi037@gmail.com',
      to: req.body.email,
      subject: req.body.subject,
      text: `MEDICAL`,
      html: `
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml">
          <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <meta name="viewport" content="width=device-width, initial-scale=1" /> <title>beauty world | payment Notification</title>
            <style type="text/css"> * { font-family: Helvetica, Arial, sans-serif; } body { -webkit-font-smoothing: antialiased;  -webkit-text-size-adjust: none; width: 100% !important; margin: 0 !important;  height: 100%; color: #676767; } td { font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #777777; text-align: center;  line-height: 21px; }  a { color: #676767; text-decoration: none !important; } .pull-left { text-align: left; } .pull-right { text-align: right; } .header-lg, .header-md,  .header-sm { font-size: 32px; font-weight: 700; line-height: normal; padding: 35px 0 0; color: #4d4d4d; } .header-md { font-size: 24px; } .header-sm { padding: 5px 0; font-size: 18px;  line-height: 1.3; } .content-padding { padding: 20px 0 5px; } .mobile-header-padding-right { width: 290px; text-align: right; padding-left: 10px; } .mobile-header-padding-left { width: 290px; text-align: left;  padding-left: 10px; } .free-text {  width: 100% !important; padding: 10px 60px 0px; } .mini-block { border: 1px solid #e5e5e5;  border-radius: 5px;  background-color: #ffffff; padding: 12px 15px 15px; text-align: left; width: 253px; } .mini-container-left { width: 278px; padding: 10px 0 10px 15px;  } .mini-container-right { width: 278px; padding: 10px 14px 10px 15px; } .product { text-align: left; vertical-align: top; width: 175px; }  .total-space {  padding-bottom: 8px; display: inline-block; } .item-table { padding: 50px 20px; width: 560px; } .item { width: 300px; } .mobile-hide-img { text-align: left;  width: 125px;} .mobile-hide-img img { border: 1px solid #e6e6e6; border-radius: 4px;  } .title-dark { text-align: left;  border-bottom: 1px solid #cccccc; color: #4d4d4d; font-weight: 700;  padding-bottom: 5px; } .item-col {  padding-top: 20px; text-align: left;  vertical-align: top; } .force-width-gmail {min-width:600px; height: 0px !important;  line-height: 1px !important; font-size: 1px !important; }
            </style> <style type="text/css" media="screen">  @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700); </style>
          </head>
          <body bgcolor="#f7f7f7">
          <table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
            <tr>
              <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
                <center>
                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr><td class="header-lg"> MEDI-SAFE </td> </tr>
            <tr>
              <td align="center" valign="top" width="100%" style="background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;">
                <center>
                  <table cellpadding="0" cellspacing="0" width="600" class="w320">
                      <tr> <td class="header-lg"><span style="color: #D24130; font-size: 28px;" >Dear ${req.body.name},</span>   </td> </tr>
                      <tr> <td class="free-text">  ${req.body.message} </td> </tr>
                      <tr> <td class="free-text">   We are working to serve you more and better. </td> </tr>
                      <tr> <td class="free-text"> <span style="color: #D24130; font-size: 20px;" >MEDI-SAFE</span> </td> </tr>
                      <tr> <td class="free-text">  Best regards.. </td>  </tr>
                  </table>
                </center>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
                <center> <table cellspacing="0" cellpadding="0" width="600" class="w320"> <tr> <td style="padding: 25px 0 25px"> <strong>MEDI-SAFE</strong><br /> </td> </tr> </table> </center>
              </td>
            </tr>
          </table> </div> </body> </html>
      `
    }
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        req.flash('danger', error.message)
        res.redirect(`/send_email_messages?staff_registration_id=${req.query.staff_registration_id}%>&email_address=${req.query.email_address}&name=${req.query.name} `)
      } else {
        req.flash('danger', 'Email Successfuly sent')
        res.redirect(`/send_email_messages?staff_registration_id=${req.query.staff_registration_id}%>&email_address=${req.query.email_address}&name=${req.query.name} `)
      }
    })
  });


route.get('/aplicant_update_details_delete_staff', async (req, res) => {
  let staff = await staffregistration.findOne({ where: { staff_id: req.query.applicantno } });
  staff.destroy().then(() => {
    req.flash('danger', 'STAFF DETAILS DESTROYED!!')
    res.redirect('back')
  })
});

route.route('/edit_staff_detais')
  .post(async (req, res) => {
    try {
      var applicant = {
        surname: req.body.surname,
        firstname: req.body.firstname,
        middlename: req.body.middlename,
        gender: req.body.gender,
        dateofbirth: req.body.dateofbirth,
        personal_phone_no: req.body.personal_phone_no,
        kin_name: req.body.kin_name,
        kin_address: req.body.kin_address,
        kin_phone: req.body.kin_phone,
        personal_phone_no: req.body.personal_phone_no,
        home_address: req.body.home_address,
        // designation: req.body.designation,
        // specialization: req.body.specialization,
        // highest_qualification: req.body.highest_qualification,
        // qualification_year: req.body.qualification_year,
        // serviceid: req.user.service_id,
        // email: req.body.email,
        // group_id: req.body.group,

      };

      console.log(req.query.staff_id)

      var staff = await staffregistration.findOne({ where: { staff_id: req.query.staff_id } });

      staff.update(applicant, { new: true }).then(() => {
        req.flash('info', 'STAFF DETAILS UPDATED')
        res.redirect('back');
      })
    } catch (err) {
      req.flash('info', err.message)
      res.redirect('back');
    }
  })
  .get(async (req, res) => {
    res.redirect('/aplicant_registration')
  });


route.route('/edit_staff_qualification')
  .get(async (req, res) => {
    res.redirect('/aplicant_registration')
  })
  .post(async (req, res) => {
    try {
      var applicant = {

        designation: req.body.designation,
        specialization: req.body.specialization,
        highest_qualification: req.body.highest_qualification,
        qualification_year: req.body.qualification_year,
      };

      console.log(req.query.staff_id)

      var staff = await staffregistration.findOne({ where: { staff_id: req.query.staff_id } });

      staff.update(applicant, { new: true }).then(() => {
        req.flash('info', 'STAFF qualification DETAILS UPDATED')
        res.redirect('back');
      })
    } catch (err) {
      req.flash('info', err.message)
      res.redirect('back');
    }
  });


route.route('/patient_profile')
  .get(async (req, res) => {
    var patid = req.query.patient;
    let patient = await Patients.findOne({ where: { PatID: patid } })
    res.render('./admin/applicant/patient_update_detail', {
      patid, patient
    })
  })
  .post(async (req, res) => {
    try {
      var applicant = {

        PatName: req.body.PatName,
        PatEmail: req.body.specialization,
        PatPhone: req.body.PatPhone,
        PaAddress: req.body.PaAddress,
      };

      console.log(req.query.staff_id)

      let patient = await Patients.findOne({ where: { PatID: req.query.patient } })

      patient.update(applicant, { new: true }).then(() => {
        req.flash('info', 'PATIENT DETAILS UPDATED')
        res.redirect('back');
      })
    } catch (err) {
      req.flash('info', err.message)
      res.redirect('back');
    }
  });

route.get('/aplicant_update_delete', async (req, res) => {
  var patid = req.query.patient;
  let patient = await Patients.findOne({ where: { PatID: patid } })
  patient.destroy().then(() => {
    req.flash('info', 'PATIENT DETAIL DESTROYED!')
    res.redirect('back');
  })
});

route.route('/update_diagnosis_details')
  .get(async (req, res) => {
    var DiagID = req.query.DiagID;
    var dia = await pat_diagnosis.findOne({ where: { DiagID: DiagID } })

    res.render('./admin/parents/edit_diagnosis', {
      dia, DiagID,
    })
  })
  .post(async (req, res) => {
    try {
      let diagDeal = {
        DiaDetail: req.body.DiaDetail,
        DiagRemark: req.body.DiagRemark,
        Description: req.body.Description,
        DiagDate: req.body.DiagDate,
      }

      var DiagID = req.query.DiagID;
      var dia = await pat_diagnosis.findOne({ where: { DiagID: DiagID } })

      dia.update(diagDeal, { new: true }).then(() => {
        req.flash('info', 'PATIENT DIAGNOSIS DETAILS UPDATED')
        res.redirect('back');
      })
    } catch (err) {
      req.flash('info', err.message)
      res.redirect('back');
    }
  });


route.get('/deleted_diagnosis_delete', async (req, res) => {
  var DiagID = req.query.DiagID;
  var dia = await pat_diagnosis.findOne({ where: { DiagID: DiagID } })
  dia.destroy().then(() => {
    req.flash('info', 'PATIENT DIAGNOSIS DETAIL DESTROYED!')
    res.redirect('back');
  })
})
module.exports = route;
