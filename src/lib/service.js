exports.isAdmin = function (req, res, next) {
    if (req.isAuthenticated() && req.user.group_id == 111111) {
        next();
      } else {
        req.session.oldUrl = req.url; 
  req.session.destroy();
        req.flash('danger', 'Please Login as admin! ');
        res.redirect('/login')
      }
}

exports.isContestant = function (req, res, next) {
  if (req.isAuthenticated() &&  req.user.group_id == 222222 ) {
      next();
    } else {
      req.session.oldUrl = req.url; 

      req.flash('danger', 'Please Login as contestant Admin!');
      res.redirect('/login')
    }
}