const Sequelize = require('sequelize');
const sequelize = new Sequelize( process.env.DATABASE, process.env.NAME, process.env.DB_PASSWORD, {
    dialect: 'mysql',
    host: process.env.DB_HOST,
    dialectOptions: {
        multipleStatements: true
      }
});

module.exports = sequelize;
