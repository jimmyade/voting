
var bcrypt = require('bcryptjs');;
const saltRounds = 10;

const { staffregistration } = require('../../src/models/staffModel/staffmodel')
// const {_lgas, _countries, _states, titletable, termsetup, subjecttable, _designations } = require('../../src/models/basicTablesModel')
const { Users, User } = require('../../src/models/usersMosel');
const { _countries, titletable, _designations, Patients, pat_diagnosis, dia, prescription, user_groups } = require('../models/lga_state_country_cities_streetModel');

module.exports.getApplicatepage = async function (req, res) {
    let applicant = await staffregistration.findAll({ order: [['staff_reg_id', 'asc']] });
    let titles = await titletable.findAll();
    let designations = await _designations.findAll()
    let usergroups = await user_groups.findAll()
    res.render('./admin/applicant/view_applicant_details', {
        applicant: applicant,
        titles, designations, usergroups
    })
}

module.exports.createApplicant = async function (req, res) {
    try {
        function staffID(len) {
            len = len || 100;
            var nuc = "0123456789";
            var i = 0;
            var n = 0;
            s = "";
            while (i <= len - 1) {
                n = Math.floor(Math.random() * 7);
                s += nuc[n];
                i++;
            }
            return s;
        }
        var staff_id = staffID(15);
        var applicant = {
            staff_id: staff_id,
            schoolId: req.user.organization_id,
            title: req.body.title,
            surname: req.body.surname,
            firstname: req.body.firstname,
            middlename: req.body.middlename,
            gender: req.body.gender,
            dateofbirth: req.body.dateofbirth,
            activity: req.body.activity,
            personal_phone_no: req.body.personal_phone_no,
            kin_name: req.body.kin_name,
            kin_address: req.body.kin_address,
            kin_phone: req.body.kin_phone,
            registered_by: req.user.name,
            date_Of_Entry: req.body.date_Of_Entry,
            personal_phone_no: req.body.personal_phone_no,
            home_address: req.body.home_address,
            designation: req.body.designation,
            specialization: req.body.specialization,
            highest_qualification: req.body.highest_qualification,
            qualification_year: req.body.qualification_year,
            serviceid: req.user.service_id,
            email: req.body.email,
            group_id: req.body.group,
            photo: req.file.filename,
        };

        let newApplicant = new staffregistration(applicant);
        newApplicant.save().then(function (result) {
            newStaff(req, res);
            req.flash('info', "staff added!")
            res.redirect("back");
        })

    } catch (e) {
        res.send(e.message)
    }
}

async function newStaff(req, res) {
    var createStaffLogin = {
        group_id: req.body.group,
        firstname: req.body.firstname,
        surname: req.body.surname,
        middlename: req.body.middlename,
        name: `${req.body.surname} ${req.body.surname} ${req.body.middlename}`,
        email: req.body.email,
        username: req.body.email,
        user_phone: req.body.phone,
        service_code: req.user.service_code,
        service_id: req.user.serviceid,
        user_phone: req.body.personal_phone_no,
        password: await bcrypt.hash('1234567', saltRounds),
        organization_id: req.user.organization_id

    }
    let newStaffLogin = new Users(createStaffLogin);
    newStaffLogin.save()

}

module.exports.viewStaffDetails = async function (req, res) {
    var staffID = req.query.applicantno;
    let staff = await staffregistration.findOne({ where: { staff_id: staffID } });
    res.render('./admin/applicant/view_staff_info', {
        staff, staffID
    })
}

module.exports.getStaffUpdatePage = async function (req, res) {
    var staffID = req.query.applicantno;
    let staff = await staffregistration.findOne({ where: { staff_id: staffID } });
    res.render('', {
        staff
    })
}

module.exports.updateStaffinfo = async function (req, res) {
    try {
        var staffID = req.query.applicantno;
        let staffinfo = {

        }
        let staff = await staffregistration.findOne({ where: { staff_id: staffID } });
        staff.update(staffinfo, { new: true }).then(result => {
            res.render('', {

            })
        })
    } catch (e) {
        req.flash('danger', e.message);
        res.redirect('');
    }

}

module.exports.deleteStaff = async function (req, res) {
    var staffID = req.query.applicantno;;
    let staff = await staffregistration.findOne({ where: { staff_id: staffID } });
    staff.destroy().then(function (result) {
        req.flash('danger', "staff info deleted!");
        res.redirect('back')
    })
}






module.exports.parentLogin = async function (req, res) {
    var titles = await titletable.findAll();
    var patients = await Patients.findAll();
 
    // console.log(pss)
    res.render('./admin/parents/create_patient_account', {
        titles, patients
    })
}

module.exports.createParentLogin = async function (req, res) {
    try {
        var text = "1234567"
        var pss = await bcrypt.hash(text, saltRounds);
        var addpatient = {
            title: req.body.title,
            PatName: req.body.name,
            PatEmail: req.body.email,
            PatPhone: req.body.user_phone,
            gender: req.body.gender,
            photo: req.file.filename,
            PaAddress: req.body.home_address,
            dob: req.body.dob,

        }

        var newPatientLogin = new Patients(addpatient);
        newPatientLogin.save().then(function (result) {
            var patientLogin = {
                title: req.body.title,
                group_id: '444444',
                name: req.body.name,
                username: req.body.email,
                password: pss,
                email: req.body.email,
                phone: req.body.user_phone,
                dob: req.body.dob,
                address: req.body.home_address,
                photo: req.file.filename,
            }
            var newPatient = new Users(patientLogin)
            newPatient.save();
            req.flash('info', "Account created Succesfully");
            res.redirect('back')
        }).catch(err => {
            // req.flash('danger', err.message);
            // res.redirect('back')
            console.log(err)
            req.flash('info', err.message);
            res.redirect('back')
        })

    } catch (e) {
        // req.flash('danger', e.message);
        // res.redirect('back')
        console.log(e)
    }
}


module.exports.getDiagnosis = async (req, res) => {
    let diagnosis = await pat_diagnosis.findAll({ where: { PatID: req.query.patient } });
    let patient = await Patients.findOne({ where: { PatID: req.query.patient } })
    res.render('./admin/parents/diagnoze_patient', {
        diagnosis, ID: req.query.patient, patient
    })
}

module.exports.addDiagnosis = async (req, res) => {
    try {
        let dia = {
            DiaDetail: req.body.DiaDetail,
            DiagRemark: req.body.DiagRemark,
            Description: req.body.Description,
            DiagDate: req.body.DiagDate,
            DiaReportDoc: req.file.filename,
            PatID: req.query.patient
        }

        let newDiaDetail = new pat_diagnosis(dia);
        newDiaDetail.save().then(result => {
            req.flash('info', 'Entered!!')
            res.redirect(`/daignosis?patient=${req.query.patient}`)
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect(`/daignosis?patient=${req.query.patient}`)
    }
}

module.exports.getPrescription = async (req, res) => {
    let pid = req.query.PatID;
    let did = req.query.DiagID;
    let diagnosis = await pat_diagnosis.findOne({ where: { DiagID: did } });
    let patient = await Patients.findOne({ where: { PatID: pid } })
    let presc = await prescription.findAll({ where: { PatID: pid, DiagID: did } })
    res.render('./admin/parents/prescriptions', {
        presc, diagnosis, patient, pid, did
    })
}

module.exports.addPrescription = async (req, res) => {
    try {
        let pid = req.query.PatID;
        let did = req.query.DiagID;
        console.log(did)
        let medicines = [];
        let items = [req.body]
        // console.log(items)

        for (item of items) {
            let medName = item.MedName;
            let medI = item.MedInstruction;
            let medP = item.MedPrice;

            // console.log(medName)

            for (let i = 0; i < medName.length; i++) {
                let MN = medName[i],
                    MI = medI[i],
                    MP = medP[i];
                let medicine = {
                    MedName: MN,
                    MedInstruction: MI,
                    MedPrice: MP,
                    DiagID: did,
                    PatID: req.query.PatID,
                };
                medicines.push(medicine)
            }
        }
        console.log(pid)
        prescription.bulkCreate(medicines).then(function (result) {
            req.flash('info', 'Medicines Added');
            res.redirect(`/prescribe_drugs?DiagID=${did}&PatID=${pid}`)
        }).catch(e => {
            req.flash('info', e.message);
            res.redirect(`/prescribe_drugs?DiagID=${did}&PatID=${pid}`)
        })




    } catch (err) {
        req.flash('info', err.message);
        res.redirect(`/prescribe_drugs?DiagID=${req.query.DiagID}&PatID=${req.query.PatID}`)
    }
}