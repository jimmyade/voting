const express = require("express");
const bodyParser = require("body-parser");
const app = express();
require('dotenv').config()

const methodOverride = require("method-override");
const path = require("path");
const cors = require("cors");
const expressLayout = require("express-ejs-layouts");
//const logger = require('morgan');
const cookieParser = require("cookie-parser");
const auth = require("./src/lib/auth");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

//local variable
app.locals.footer = new Date().getFullYear();
app.locals.title = "MEDI-SAFE";

//MySql Store setup
const options = {
  host: process.env.DB_HOST,
  user: process.env.NAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DATABASE
};
const sessionStore = new MySQLStore(options);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());
app.use(methodOverride("_method"));
app.use(expressLayout);
app.use(cookieParser());

//session setup
app.set("trust proxy", 1); // trust first proxy
app.use(
  session({
    secret: "kkjkkljkjjl",
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 365,
    },
  })
);

//passport authentication
// app.use(passport.initialize());
// app.use(passport.session());

app.use(auth.initialize);
app.use(auth.session);
app.use(auth.setUser);

//flash alert middleware 
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//static for css js and other styling files
app.use(express.static("./public"));
app.use(express.static("./uploads"));



//include routers pages
const appUsers = require("./src/routes/users/index");
const route = require("./src/routes/routers");
const paystacts = require('./src/routes/payment/paystackPaymentRoute');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;


//setting app to render html path
app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');

app.use("*", function(req, res, next){
  res.locals.islogin = req.isAuthenticated();
  res.locals.login =  req.user;
  res.locals.link = req.originalUrl;
  
  next();
})

//routes(app);
app.use(appUsers);
// app.use(userRoute);
app.use(route);
app.use(paystacts);
// app.use(teachersRoute);

// //error handling
// app.use(function(req, res, next){
//   const err = new Error("Not found");
//   err.status = 404;
//   next(err);
//  });
//  app.use((err, req, res, next)=>{
//    res.status(err.status || 500);
//    res.render('error',{
//      error:{
//        status: err.status || 500,
//        message: err.message
//      }
//    })
//  })

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`App running at port ${PORT}`);
});
